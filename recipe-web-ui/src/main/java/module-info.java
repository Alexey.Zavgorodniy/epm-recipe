open module recipe.web.ui {
    requires recipe.services.api;
    requires recipe.security;
    requires spring.context;
    requires spring.web;
    requires spring.webmvc;
    requires thymeleaf.spring5;
    requires thymeleaf;
    requires thymeleaf.extras.springsecurity5;
    exports com.epm.recipe.web_ui.config;
}