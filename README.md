# HOMEWORK 21
_____

### Description
In this homework, I added Spring Security implemented in module recipe-security.

Start page mapping: **http://localhost/index**.

Request mapping starts at **http://localhost/api/**, and have few suffixes:

* **recipe_of_the_day** - finds recipe of the day (GET method);
* **recipe** - finds all recipes (GET method);
* **recipe/{id}** - finds one recipe by id (GET method);
* **recipe** - creates new recipe (POST method, which has new recipe title in its body);
* **recipe/{id}** - updates existing recipe by id (PUT method, which has new recipe title in its body);
* **recipe/{id}** - deletes existing recipe by id (DELETE method).

Link to shared **Postman Collection**:
https://www.getpostman.com/collections/d7c6e225e2b78c20ed78

