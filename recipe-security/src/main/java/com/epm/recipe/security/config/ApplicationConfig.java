package com.epm.recipe.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public SecurityConfiguration securityConfiguration(
            LoggingAccessDeniedHandler accessDeniedHandler) {
        return new SecurityConfiguration(accessDeniedHandler);
    }

    @Bean
    public LoggingAccessDeniedHandler loggingAccessDeniedHandler() {
        return new LoggingAccessDeniedHandler();
    }
}