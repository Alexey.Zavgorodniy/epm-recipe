module recipe.security {
    requires javax.servlet.api;
    requires spring.security.config;
    requires spring.security.core;
    requires spring.security.web;
    requires spring.context;
    requires spring.beans;
    opens com.epm.recipe.security.config to spring.core;
    exports com.epm.recipe.security.config;
}